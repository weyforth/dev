var isFullScreen = function(win){
  return win.rect().width >= win.screen().rect().width - 10;
};


// var isRight = function(win){
//   slate.log(win.screen().rect().width / 2);
//   slate.log(win.rect().x);
//   return win.rect().x == win.screen().rect().width;
// };


// var isLeft = function(win){
//   return win.rect().x == 0;
// };


var isTopLeft = function(win){

};


var isBottomLeft = function(win){

};


var isTopRight = function(win){

};


var isBottomRight = function(win){

};


var canMoveScreenLeft = function(win){
  return win.screen().id() != 0;
}


var canMoveScreenRight = function(win){
  return win.screen().id() != slate.screenCount() - 1;
}


var movetoFullScreenLeft = function(win){
  win.doOperation(
    slate.operation(
      "move", {
        "x" : "screenOriginX",
        "y" : "screenOriginY",
        "width" : "screenSizeX",
        "height" : "screenSizeY",
        "screen" : win.screen().id() - 1
      }
    )
  );
};


var movetoFullScreenRight = function(win){
  win.doOperation(
    slate.operation(
      "move", {
        "x" : "screenOriginX",
        "y" : "screenOriginY",
        "width" : "screenSizeX",
        "height" : "screenSizeY",
        "screen" : win.screen().id() + 1
      }
    )
  );
};


var movetoHalfScreenRight = function(win){
  var rectBefore = win.rect();

  win.doOperation(
    slate.operation(
      "push", {
        "direction" : "right",
        "style" : "bar-resize:screenSizeX/2",
      }
    )
  );

  if(win.rect().equals(rectBefore) && canMoveScreenRight(win)){
    win.doOperation(
      slate.operation(
        "push", {
          "direction" : "left",
          "style" : "bar-resize:screenSizeX/2",
          "screen" : win.screen().id() + 1,
        }
      )
    );
  }
};


var movetoHalfScreenLeft = function(win){
  var rectBefore = win.rect();

  win.doOperation(
    slate.operation(
      "push", {
        "direction" : "left",
        "style" : "bar-resize:screenSizeX/2",
      }
    )
  );

  if(win.rect().equals(rectBefore) && canMoveScreenLeft(win)){
    win.doOperation(
      slate.operation(
        "push", {
          "direction" : "right",
          "style" : "bar-resize:screenSizeX/2",
          "screen" : win.screen().id() - 1,
        }
      )
    );
  }
};


var goFullScreen = function(win){
  win.doOperation(
    slate.operation(
      "move", {
        "x" : "screenOriginX",
        "y" : "screenOriginY",
        "width" : "screenSizeX",
        "height" : "screenSizeY",
      }
    )
  );
}

slate.bind("left:cmd,shift", function(win) {
  if(!win) return;

  if(isFullScreen(win) && canMoveScreenLeft(win)){
    movetoFullScreenLeft(win);
  }else{
    movetoHalfScreenLeft(win);
  }
});


slate.bind("right:cmd,shift", function(win) {
  if(!win) return;

  if(isFullScreen(win) && canMoveScreenRight(win)){
    movetoFullScreenRight(win);
  }else{
    movetoHalfScreenRight(win);
  }
});


slate.bind("up:cmd,shift", function(win) {
  if(!win) return;

  goFullScreen(win);
});


slate.bindAll({
  "0:cmd,shift": slate.operation("relaunch"),
  "pad0:cmd,shift": slate.operation("relaunch")
});


Object.prototype.equals = function(x){
  var p;
  for(p in this) {
      if(typeof(x[p])=='undefined') {return false;}
  }

  for(p in this) {
      if (this[p]) {
          switch(typeof(this[p])) {
              case 'object':
                  if (!this[p].equals(x[p])) { return false; } break;
              case 'function':
                  if (typeof(x[p])=='undefined' ||
                      (p != 'equals' && this[p].toString() != x[p].toString()))
                      return false;
                  break;
              default:
                  if (this[p] != x[p]) { return false; }
          }
      } else {
          if (x[p])
              return false;
      }
  }

  for(p in x) {
      if(typeof(this[p])=='undefined') {return false;}
  }

  return true;
}
